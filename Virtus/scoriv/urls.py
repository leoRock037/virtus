from django.conf.urls import patterns, url, include
from django.contrib import admin

urlpatterns = patterns('scoriv.views',
	
	url(r'^$', 'indexView',name="index"),
	url(r'^home$', 'home' ,name='home'),

    url(r'^createuser/$', 'user' ,name='user'),
	url(r'^createuser/user$', 'createUser' ,name='createUser'),

)
