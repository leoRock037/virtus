from django.shortcuts import render
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required


from django.conf.urls import patterns, include, url


# Create your views here.


def indexView(request):
    return render(request, 'index.html')

@login_required()
def home(request):
    return render(request, 'home.html',{'range': range(24)})

def user(request):
	return render(request,'create_user.html')

def createUser(request):
	username = request.POST['username']
	password = request.POST['password']
	email = request.POST['email']
	user = User.objects.create_user(username, email, password)	
	user.save()
	return HttpResponseRedirect(reverse('django.contrib.auth.views.login'))