from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'Virtus.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^',include('scoriv.urls')),

    url(r'^login/', 'django.contrib.auth.views.login',{'template_name': 'login.html'}, name='login'),
    url(r'^logout/', 'django.contrib.auth.views.logout_then_login', name='logout'),
)

